export default function (component, oldData, newData) {
  for (const k in oldData) {
    component.$delete(oldData, k)
  }
  for (const k in newData) {
    component.$set(oldData, k, newData[k])
  }
}
